#pragma once
#include <fstream>
#include <exception>
#include <iostream>

using namespace std;
/*******************************************************************************/
class CannotOpenAFileExcpetion : public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Nie mozna otworzyc pliku";
	}
};

class BadInputFileException : public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Zly plik wejsciowy";
	}
};
/*********************************************************************************/
//Otw�rz plik do zapisu
//true - poprane otwarcie
//false - b�ad otwarcia pliku
bool otworzDoZapisu(ofstream & plik_wy, string nazwa)
{
	plik_wy.open(nazwa, ios_base::out | ios_base::app);
	if (!plik_wy.good()) {
		//cerr << "Nie mozna otworzyc pliku" << endl;
		return false;
	}
	return true;
}

//Otw�rz plik o odczytu
//true - poprane otwarcie
//false - b�ad otwarcia pliku
bool otworzDoOdczytu(ifstream & plik_we, string nazwa)
{
	plik_we.open(nazwa, ios_base::in);
	if (!plik_we.good()) {
		//cerr << "Nie mozna owtorzyc pliku" << endl;
		return false;
	}
	return true;
}

//zamyka plik, ktory zostal otwarty w trybie do odczytu
//param [in/out] plik - plik do odczytu, ktory zostanie zamkniety
void zamknijPlikDoOdczytu(std::ifstream& plik)
{
	plik.close();
}

//zamyka plik, ktory zostal otwarty w trybie do zapisu
//param [in/out] plik - plik do zosisu, ktory zostanie zamkniety
void zamknijPlikDoZapisu(std::ofstream& plik)
{
	plik.close();
}

//generuje graf o zadanej liczbie wierzcholkow i gestosci, krawedzie sa wypelniane losowo
//param [out] graf - referencja do grafu, ktory chcemy wygenerowac
//param [in] liczbaWierzcholkow - ilosc wierzcholkow generowanego grafu
//param [in] gestosc - stosunek krawedzi do wszystkich mo�liwych krawedzi
template <class typGrafu>
void generujGraf(typGrafu & graf, int liczbaWierzcholkow, float gestosc)
{
	int j = 1, k = 0, l = 0; //liczniki

	int liczbaKrawedzi = ((gestosc / 100)*liczbaWierzcholkow*(liczbaWierzcholkow - 1)) / 2;
	for (int i = 1; i <= liczbaWierzcholkow; i++) {  //dodanie wszystkich wierzcholkow
		graf.insertVertex(i);
	}
	for (int i = 0; i < liczbaKrawedzi; i++) { //dodawanie krawedzi
		if (j%liczbaWierzcholkow == 0) l++; //zwiekszenie licznika odpowiadaj�cego za odleg�osc wierzcholkow
		j = (i%liczbaWierzcholkow) + 1;  //wierzcholek poczatkowy
		k = ((j%liczbaWierzcholkow) + l) % liczbaWierzcholkow + 1; //wierzcholek koncowy
		graf.insertEdge(j, k, rand() % (2 * liczbaKrawedzi) + 1);
	}
}

//generuje bezposrednio plik z grafem (w odpowiednim sposobie zapisu)
//param [in] nazwa - nazwa pliku do utworzenia
//param [out] liczbaWierzcholkow - ilosc wierzcholkow generowanego grafu
//param [in] gestosc - stosunek krawedzi do wszystkich mo�liwych krawedzi
void generujGrafDoPliku(string nazwa, int liczbaWierzcholkow, float gestosc)
{
	ofstream plikWy;
	int liczbaKrawedzi = ((gestosc / 100)*liczbaWierzcholkow*(liczbaWierzcholkow - 1)) / 2;
	int j = 1, k = 0, l = 0; //liczniki
	if (otworzDoZapisu(plikWy, nazwa)) {
		plikWy << liczbaWierzcholkow << " " << liczbaKrawedzi << endl;
		for (int i = 1; i <= liczbaWierzcholkow; i++)  //zapisanie wszystkich krawedzi do pliku
			plikWy << i << " ";
		plikWy << "\n0\n"; //znak poczatku danych zawierajacych krawedzie (wierzcho�ek nie moze by� zerem)
		for (int i = 0; i < liczbaKrawedzi; i++) { //zapisanie krawedzi z wierzcholkami
			if (j%liczbaWierzcholkow == 0) l++; //zwiekszenie licznika odpowiadaj�cego za odleg�osc wierzcholkow
			j = (i%liczbaWierzcholkow) + 1;  //wierzcholek poczatkowy
			k = ((j%liczbaWierzcholkow) + l) % liczbaWierzcholkow + 1; //wierzcholek koncowy
			plikWy << j << " " << k << ":" << rand() % (2 * liczbaKrawedzi) + 1 << endl;
		}
	}
	else throw CannotOpenAFileExcpetion();
	zamknijPlikDoZapisu(plikWy);
}

//zapisuje graf do pliku w odpowienim systemie zapisu
//param [in] graf - graf, ktory chcemy zapisac do pliku
//param [in] nazwa - nazwa pliku, w ktorym zapisuje
template <class typGrafu>
void zapiszGrafDoPliku(typGrafu & graf, string nazwa)
{
	ofstream plikWy;

	if (otworzDoZapisu(plikWy, nazwa)) {
		plikWy << graf.getWierzcholki().size() << " " << graf.getKrawedzie().size() << endl;
		for (int i = 0; i < graf.getWierzcholki().size(); i++)  //zapisanie wszystkich krawedzi do pliku
			plikWy << graf.getWierzcholki().atIndex(i)->getWartosc() << " ";
		plikWy << "\n0\n"; //znak poczatku danych zawierajacych krawedzie (wierzcho�ek nie moze by� zerem)
		for (int i = 0; i < graf.getKrawedzie().size(); i++) {  //zapisanie krawedzi z wierzcholkami
			int * tab = graf.endVertices(graf.getKrawedzie().atIndex(i));
			plikWy << tab[0] << " " << tab[1] << ":"
				<< graf.getKrawedzie().atIndex(i)->getWartosc() << endl;
		}
	}
	else throw CannotOpenAFileExcpetion();
	zamknijPlikDoZapisu(plikWy);
}

//generuje wszystkie pliki z grafami, dla wszystkich mozliwosci
template <class typGrafu>
void generujWszystkiePliki(int iloscGrafow, vector <int> liczbaWierzcholkow, vector <float> gestosc)
{
	string nazwa;

	for (int k = 0; k < iloscGrafow; k++) {
		for (vector<int>::iterator i = liczbaWierzcholkow.begin(); i != liczbaWierzcholkow.end(); ++i) {
			for (vector<float>::iterator j = gestosc.begin(); j != gestosc.end(); ++j) {
				nazwa = to_string(*i) + "_" + to_string(*j) + "(" + to_string(k) + ").txt";  //generowanie nazwy
				generujGrafDoPliku(nazwa, *i, *j);
			}
		}
	}
}

//wczytuje graf z pliku
//param [in] nazwa - nazwa pliku do odczytu
//param [out] graf - referencja do grafu, do ktorego chcemy wczytac
template <class typGrafu>
void wczytajPlik(string nazwa, typGrafu & graf)
{
	ifstream plikWe;
	int tmp1, tmp2, tmp3; //pomocnicze zmienne tymczasowe
	int iloscwierzcholkow, iloscKrawedzi;
	char zk; //znak krawedzi
	if (otworzDoOdczytu(plikWe, nazwa)) {
		plikWe >> iloscwierzcholkow >> iloscKrawedzi;
		plikWe >> tmp1;
		for (int i = 0; i < iloscwierzcholkow; i++) {  //wczytanie wierzcholkow
			graf.insertVertex(tmp1);
			plikWe >> tmp1;
		}
		for (int i = 0; i < iloscKrawedzi; i++) {  //wczytywanie krawedzi
			plikWe >> tmp1 >> tmp2 >> zk;
			if (zk == ':') plikWe >> tmp3;
			else throw BadInputFileException();
			graf.insertEdge(tmp1, tmp2, tmp3);
		}
	}
	else throw CannotOpenAFileExcpetion();
	zamknijPlikDoOdczytu(plikWe);
}
