#pragma once
#include <exception>
#include "lista.h"

using namespace std;

template <class typWierzcholka, class typKrawedzi> class wierzcholek;
template <class typWierzcholka, class typKrawedzi> class krawedz;
template <class typWierzcholka, class typKrawedzi> class grafMacierz;

/**********************************************************************************/
class UndefinedConnectionException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Podane elementy nie sa ze soba powiazane";
	}
};

class FullMatrixException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Nie mozna dodac wiecej wierzcholkow";
	}
};

class AlreadyExistException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Podany element juz istnieje";
	}
};

class DoesNotExistException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Element nie istnieje";
	}
};

class BeginNotExistException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Wezel poczatkowy nie istnieje";
	}
};

class EndNotExistException :public exception
{
public:
	virtual const char * what() const noexcept {
		return "Wezel koncowy nie istnieje";
	}
};
/*************************************************************************************/

template <class typWierzcholka, class typKrawedzi>
class wierzcholek
{
	int klucz;
	typWierzcholka wartosc;
	static int licznik;
	element <wierzcholek<typWierzcholka, typKrawedzi>*>* elem; //wska�nik na miejsce w liscie wierzcholkow
public:
	wierzcholek(typWierzcholka w = 0) : wartosc(w) {
		klucz = licznik;
		licznik++;
		elem = nullptr;
	}

	const int getKlucz() { return klucz; }
	const typWierzcholka & getWartosc() const { return wartosc; }
	const typWierzcholka & getWartosc() { return wartosc; }

	wierzcholek <typWierzcholka, typKrawedzi> * nextVertex() { if (elem->nastepny) return elem->nastepny->wartosc; }

	static void zminiejszLicznik() { licznik--; }
	static void zerujLicznik() { licznik = 0; }
	static int getLicznik() { return licznik; }

	friend class krawedz <typWierzcholka, typKrawedzi>;
	friend class grafMacierz <typWierzcholka, typKrawedzi>;
};

template <class typWierzcholka, class typKrawedzi>
int wierzcholek<typWierzcholka, typKrawedzi>::licznik = 0;

template <class typWierzcholka, class typKrawedzi>
class krawedz
{
	typKrawedzi wartosc;
	wierzcholek <typWierzcholka, typKrawedzi> *poczatek; //wska�nik na pocz�tkowy weze�
	wierzcholek <typWierzcholka, typKrawedzi> *koniec; //wska�nik na koncowy wierzcholek
	element <krawedz<typWierzcholka, typKrawedzi>*>* elem; //wska�nik na miejsce w liscie krawedzi

public:
	krawedz(typKrawedzi w = 0, wierzcholek<typWierzcholka, typKrawedzi>*pocz = 0, wierzcholek <typWierzcholka, typKrawedzi>*kon = 0) :
		wartosc(w), poczatek(pocz), koniec(kon) {}
	const typKrawedzi getWartosc() const { return wartosc; }
	const typKrawedzi getWartosc() { return wartosc; }
	const wierzcholek <typWierzcholka, typKrawedzi> *getPoczatek() { return poczatek; }
	const wierzcholek <typWierzcholka, typKrawedzi> *getKoniec() { return koniec; }

	krawedz <typWierzcholka, typKrawedzi> * nextEdge() { if (elem->nastepny) return elem->nastepny->wartosc; }

	friend class wierzcholek <typWierzcholka, typKrawedzi>;
	friend class grafMacierz <typWierzcholka, typKrawedzi>;
};

template <class typWierzcholka, class typKrawedzi>
class grafMacierz
{
	int wielkosc; //maksymalna ilosc wierzcholkow
	int uzytaMacierz; //ilosc elementow (wierzcholkow) ju� dodanych do macierzy
	krawedz <typWierzcholka, typKrawedzi> ***macierzSasiedztwa;
	lista <int> usunieteWierzcholki; //przechowuje klucze usunietych wierzcholkow
	lista <wierzcholek<typWierzcholka, typKrawedzi>*> wierzcholki; //lista wez�ow grafu
	lista <krawedz<typWierzcholka, typKrawedzi>*> krawedzie;  //lista krawedzi grafu

public:
	grafMacierz(int w = 1000);
	~grafMacierz();
	const lista <krawedz<typWierzcholka, typKrawedzi>*> & getKrawedzie() const { return krawedzie; }
	const lista <krawedz<typWierzcholka, typKrawedzi>*> & getKrawedzie() { return krawedzie; }
	const lista <wierzcholek<typWierzcholka, typKrawedzi>*> & getWierzcholki() const { return wierzcholki; }
	const lista <wierzcholek<typWierzcholka, typKrawedzi>*> & getWierzcholki() { return wierzcholki; }

	//zwraca indeks podanego wezla lub -1, je�li nie znaleziono podanego obiektu
	int findIndxV(const typWierzcholka &obiekt);
	int findIndxV(const typWierzcholka &obiekt) const;
	int findIndxV(const wierzcholek <typWierzcholka, typKrawedzi> * obiekt);
	int findIndxV(const wierzcholek <typWierzcholka, typKrawedzi> * obiekt) const;
	//zwraca indeks podanej krawedzi lub -1, je�li nie znaleziono podanego obiektu
	int findIndxE(const typKrawedzi &obiekt);
	int findIndxE(const typKrawedzi &obiekt) const;
	int findIndxE(const krawedz <typWierzcholka, typKrawedzi> * obiekt);
	int findIndxE(const krawedz <typWierzcholka, typKrawedzi> * obiekt) const;

	//zwraca true, gdy dodano juz maksymalna ilosc wierzcholkow
	bool isFull() { return wielkosc == wierzcholek<typWierzcholka, typKrawedzi>::getLicznik(); }
	//zwraca aktualnie wykorzystywana wielkosc macierzy
	int usedMatrix() { return uzytaMacierz; }
	int usedMatrix() const { return uzytaMacierz; }
	//dodaje weze� o podanym obiekcie do grafu
	void insertVertex(const typWierzcholka &obiekt);
	//usuwa wierzcholek zawierajacy podany obiekt wraz ze wszystkimi powiazanymi krawedziami
	void removeVertex(const typWierzcholka &obiekt);
	//dodaje krawedz (pocz,kon) zawieraj�c� obiekt
	void insertEdge(const typWierzcholka &pocz, const typWierzcholka &kon, const typKrawedzi & obiekt);
	//usuwa podany wierzcholek z grafu
	void removeEdge(const typKrawedzi & obiekt);
	//zwraca tablice dwoch koncowych wierzcholkow krawedzi
	typWierzcholka * endVertices(const typKrawedzi &obiekt);
	typWierzcholka * endVertices(const krawedz <typWierzcholka, typKrawedzi> * obiekt);
	//zwraca wartosc przeciwleg�ego wierzcho�ka do "w" wzlgedem "k"
	typWierzcholka opposite(const typWierzcholka & w, const typKrawedzi & k);
	typWierzcholka opposite(const typWierzcholka & w, const krawedz <typWierzcholka, typKrawedzi> *k) const;
	//sprawdza, czy wierzcholki v i w sa sasiednie
	bool areAdjacent(const typWierzcholka & v, const typWierzcholka & w);
	//zastepuje element w wierzcholku "w" na "x"
	void replaceVertex(const typWierzcholka & w, const typWierzcholka & x);
	//zastepuje element w krawedzi "k" na "x"
	void replaceEdge(const typKrawedzi & k, const typKrawedzi &x);
	//wyswietla wszystkie wierzcholki na standardowe wyjscie
	void vertices();
	//wyswietla wszystkie krawedzie na standardowe wyjscie
	void edges();
	//wyswietla wszystkie krawedzie przylegajace do wierzcholka "w"
	void incidentEdges(const typWierzcholka & w);
	//zapisuje wszystkie krawedzie przylegajace do wierzcholka "w" do listy "l"
	void incidentEdges(const typWierzcholka & w, lista<krawedz<int, int>*>& l) const;
	//metoda pomocniczna przy debugowaniu
	//wyswietla macierz na standardowe wyjscie
	void showMatrix();
};

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxV(const typWierzcholka & obiekt)
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]->wartosc) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxV(const typWierzcholka & obiekt) const
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]->wartosc) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
inline int grafMacierz<typWierzcholka, typKrawedzi>::findIndxE(const typKrawedzi & obiekt)
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]->wartosc) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxE(const typKrawedzi & obiekt) const
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]->wartosc) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxV(const wierzcholek<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxV(const wierzcholek<typWierzcholka, typKrawedzi>* obiekt) const
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxE(const krawedz<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafMacierz<typWierzcholka, typKrawedzi>::findIndxE(const krawedz<typWierzcholka, typKrawedzi>* obiekt) const
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
grafMacierz<typWierzcholka, typKrawedzi>::grafMacierz(int w) {
	wierzcholek<typWierzcholka, typKrawedzi>::zerujLicznik();
	wielkosc = w;
	uzytaMacierz = 0;
	macierzSasiedztwa = new krawedz <typWierzcholka, typKrawedzi> **[wielkosc];
	for (int i = 0; i < wielkosc; i++)
		macierzSasiedztwa[i] = new krawedz <typWierzcholka, typKrawedzi> *[wielkosc];
	for (int i = 0; i < wielkosc; i++) {
		for (int j = 0; j < wielkosc; j++)
			macierzSasiedztwa[i][j] = nullptr;
	}
}

template<class typWierzcholka, class typKrawedzi>
grafMacierz<typWierzcholka, typKrawedzi>::~grafMacierz() {
	for (int i = 0; i < wielkosc; i++)
		delete[] macierzSasiedztwa[i];
	delete[] macierzSasiedztwa;
	wielkosc = uzytaMacierz = 0;
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::insertVertex(const typWierzcholka & obiekt)
{
	if (isFull()) throw FullMatrixException();
	else if (findIndxV(obiekt) >= 0) throw AlreadyExistException();
	else {
		wierzcholek<typWierzcholka, typKrawedzi> *nowy = new wierzcholek<typWierzcholka, typKrawedzi>(obiekt);
		if (!usunieteWierzcholki.empty()) {
			nowy->klucz = usunieteWierzcholki.pop_front();
			wierzcholek<typWierzcholka, typKrawedzi>::zminiejszLicznik();
		}
		element <wierzcholek<typWierzcholka, typKrawedzi>*>* tmp = wierzcholki.push_back(nowy);
		nowy->elem = tmp; //dodanie wskaznika na element w liscie
		uzytaMacierz++;
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::removeVertex(const typWierzcholka & obiekt)
{
	int indx = findIndxV(obiekt);
	if (indx < 0) throw DoesNotExistException();
	else {
		int k = wierzcholki[indx]->klucz;
		//usuwanie powiazanych krawedzi
		for (int i = 0; i < usedMatrix(); i++) { //przeszukaj ca�y wiersz o podanym kluczu
			if (macierzSasiedztwa[k][i] != nullptr) {  //jesli istnieje krawedz
				int indxKrawedzi = krawedzie.indexOf(macierzSasiedztwa[k][i]); //znajdz jej indeks w liscie krawedzi
				krawedzie.del(indxKrawedzi);  //usun krawedz
				macierzSasiedztwa[k][i] = nullptr; //wyzeruj wskazniki na krawedzie
				macierzSasiedztwa[i][k] = nullptr;
			}
		}
		wierzcholki.del(indx);
		usunieteWierzcholki.push_back(k); //dodanie klucza usunietego wierzcholka do bazy
		uzytaMacierz--;
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::insertEdge(const typWierzcholka & pocz, const typWierzcholka & kon, const typKrawedzi & obiekt)
{
	int indxPocz = findIndxV(pocz);
	int indxKon = findIndxV(kon);
	krawedz<typWierzcholka, typKrawedzi> *nowy = new krawedz <typWierzcholka, typKrawedzi>;
	if (indxPocz < 0) throw BeginNotExistException();
	else if (indxKon < 0) throw EndNotExistException();
	else {
		nowy->wartosc = obiekt;
		nowy->poczatek = wierzcholki[indxPocz]; //ustawienie wezla poczatkowego
		nowy->koniec = wierzcholki[indxKon]; //ustawienie wezla koncowego
		macierzSasiedztwa[wierzcholki[indxPocz]->klucz][wierzcholki[indxKon]->klucz] = nowy; //dodanie do macierzy sasiedztwa
		macierzSasiedztwa[wierzcholki[indxKon]->klucz][wierzcholki[indxPocz]->klucz] = nowy;
		element <krawedz<typWierzcholka, typKrawedzi>*>* tmp = krawedzie.push_back(nowy); //dodanie krawedzi do listy
		nowy->elem = tmp; //dodanie wskaznika na element w liscie
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::removeEdge(const typKrawedzi & obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		int kluczPocz = krawedzie[i]->poczatek->klucz;
		int kluczKon = krawedzie[i]->koniec->klucz;
		macierzSasiedztwa[kluczPocz][kluczKon] = nullptr; //zerowanie miejsc w macierzy
		macierzSasiedztwa[kluczKon][kluczPocz] = nullptr;
		krawedzie.del(i);
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka * grafMacierz<typWierzcholka, typKrawedzi>::endVertices(const typKrawedzi & obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		typWierzcholka * tab = new typWierzcholka[2];
		tab[0] = krawedzie[i]->poczatek->wartosc;
		tab[1] = krawedzie[i]->koniec->wartosc;
		return tab;
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka * grafMacierz<typWierzcholka, typKrawedzi>::endVertices(const krawedz<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		typWierzcholka * tab = new typWierzcholka[2];
		tab[0] = krawedzie[i]->poczatek->wartosc;
		tab[1] = krawedzie[i]->koniec->wartosc;
		return tab;
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka grafMacierz<typWierzcholka, typKrawedzi>::opposite(const typWierzcholka & w, const typKrawedzi & k)
{
	int indxW = findIndxV(w);
	int indxK = findIndxE(k);
	if (indxW < 0 || indxK < 0) DoesNotExistException();
	else {
		if (krawedzie[indxK]->poczatek->wartosc == w) return krawedzie[indxK]->koniec->wartosc;
		else if (krawedzie[indxK]->koniec->wartosc == w) return krawedzie[indxK]->poczatek->wartosc;
		else throw UndefinedConnectionException(); //podany wierzcholek i krawedz istnieja, ale nie sa powiazane
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka grafMacierz<typWierzcholka, typKrawedzi>::opposite(const typWierzcholka & w, const krawedz<typWierzcholka, typKrawedzi>* k) const
{
	if (k->poczatek->wartosc == w) return k->koniec->wartosc;
	else if (k->koniec->wartosc == w) return k->poczatek->wartosc;
	else throw UndefinedConnectionException(); //podany wierzcholek i krawedz istnieja, ale nie sa powiazane
}

template<class typWierzcholka, class typKrawedzi>
bool grafMacierz<typWierzcholka, typKrawedzi>::areAdjacent(const typWierzcholka & v, const typWierzcholka & w)
{
	int indxV = findIndxV(v);
	int indxW = findIndxV(w);
	if (indxV < 0 || indxW < 0) throw DoesNotExistException();
	else {
		for (int i = 0; i < usedMatrix(); i++) {
			krawedz <typWierzcholka, typKrawedzi> * tmp = macierzSasiedztwa[wierzcholki[indxV]->klucz][i];
			if (tmp != nullptr)
				if (tmp->poczatek->wartosc == w || tmp->koniec->wartosc == w) return true;
		}
	}
	return false;
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::replaceVertex(const typWierzcholka & w, const typWierzcholka & x)
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else wierzcholki[i]->wartosc = x;
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::replaceEdge(const typKrawedzi & k, const typKrawedzi & x)
{
	int i = findIndxE(k);
	if (i < 0) throw DoesNotExistException();
	else krawedzie[i]->wartosc = x;
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::vertices()
{
	for (int i = 0; i < wierzcholki.size(); i++)
		cout << wierzcholki[i]->wartosc << " ";
}

template<class typWierzcholka, class typKrawedzi>
inline void grafMacierz<typWierzcholka, typKrawedzi>::edges()
{
	for (int i = 0; i < krawedzie.size(); i++)
		cout << krawedzie[i]->wartosc << " ";
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::incidentEdges(const typWierzcholka & w)
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else {
		int kluczW = wierzcholki[i]->klucz;
		for (int j = 0; j < usedMatrix(); j++) {
			if (macierzSasiedztwa[kluczW][j] != nullptr) {
				cout << macierzSasiedztwa[kluczW][j]->wartosc << " ";
			}
		}
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::incidentEdges(const typWierzcholka & w, lista<krawedz<int, int>*>& l) const
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else {
		int kluczW = wierzcholki[i]->klucz;
		for (int j = 0; j < usedMatrix(); j++) {
			if (macierzSasiedztwa[kluczW][j] != nullptr) {
				l.push_back(macierzSasiedztwa[kluczW][j]);
			}
		}
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafMacierz<typWierzcholka, typKrawedzi>::showMatrix()
{
	for (int i = 0; i < usedMatrix(); i++) {
		for (int j = 0; j < usedMatrix(); j++) {
			if (macierzSasiedztwa[i][j] != nullptr) cout << macierzSasiedztwa[i][j]->wartosc << " ";
			else cout << "0" << " ";
		}
		cout << endl;

	}
	cout << "\nLicznik: " << wierzcholki.front()->licznik << endl;

	for (int i = 0; i < wierzcholki.size(); i++)
	{
		cout << "WARTOSC: " << wierzcholki[i]->wartosc << endl;
		cout << "KLUCZ: " << wierzcholki[i]->klucz << endl;
	}
}