//klaster.h - definiuje strukture zbior�w roz�acznych typu int
#pragma once
class klaster
{
	int * elementy;
	int iloscElem;
public:
	klaster(int iE)
	{
		iloscElem = iE;
		elementy = new int[iloscElem];
		for (int i = 0; i < iloscElem; i++)
			elementy[i] = i;
	}

	//zwraca reprezentatna klastra
	int findRep(int elem) { return elementy[elem]; }

	//�aczy ze soba dwa klastry, w ktorych znajduja sie elementy "elem1" i "elem2"
	void mergeClusters(int elem1, int elem2);
};

void klaster::mergeClusters(int elem1, int elem2)
{
	int reprezentatElem1 = findRep(elem1);
	int reprezentatElem2 = findRep(elem2);
	if (reprezentatElem1 != reprezentatElem2) {
		for (int i = 0; i < iloscElem; i++)
			if (elementy[i] == reprezentatElem2) elementy[i] = reprezentatElem1;
	}
}

