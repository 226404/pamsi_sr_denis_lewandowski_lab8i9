// algorytmy_grafow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <string>
#include <climits>
#include "kolejka_kopiec.h"
#include "klaster.h"
#include "genObslPlikowGrafow.h"
#include "Timer.h"
//#include "grafLista.h"
#include "grafMacierz.h"

using namespace std;

enum typAlgorytmu { kruskal, prima, dijkstra };

//tworzy minimalne drzewo rozpinajace za pomoca algorytmu Kruskala
//param [in] graf - graf, ktorego chcemy wyznaczyc MDR
//param [out] drzewoRozpinajace - referencja do minimalnego drzewa rozpinajacego grafu
template <class typGrafu>
void Kruskal(const typGrafu & graf, typGrafu & drzewoRozpinajace)
{
	int iloscWierzcholkow = graf.getWierzcholki().size();
	kolejka <krawedz<int, int>*> kol(graf.getKrawedzie().size() + 1);  //kolejka do sortowania krawedzi
	klaster klasterWierzcholkow(iloscWierzcholkow + 1);  //indeks 0, nie jest uzywany
	krawedz <int, int> * krawPom = graf.getKrawedzie().atIndex(0);
	for (int i = 0; i < graf.getKrawedzie().size(); i++) {//dodanie krawedzi do kolejki
		kol.insert(krawPom->getWartosc(), krawPom);
		krawPom = krawPom->nextEdge();
	}

	while (drzewoRozpinajace.getKrawedzie().size() < iloscWierzcholkow - 1) {
		krawedz <int, int> *k = kol.front();  //pobranie krawedzi o najmniejszym kluczu z kolejki
		kol.removeMin();
		int pocz = k->getPoczatek()->getWartosc();
		int kon = k->getKoniec()->getWartosc();
		if (klasterWierzcholkow.findRep(pocz) != klasterWierzcholkow.findRep(kon)) {  //je�eli wierzcholki naleza do roznych klastrow
			if (drzewoRozpinajace.findIndxV(pocz) < 0) drzewoRozpinajace.insertVertex(pocz);  //jezeli nie ma wierzcholka poczatkowego w drzewie
			if (drzewoRozpinajace.findIndxV(kon) < 0) drzewoRozpinajace.insertVertex(kon);  //jezeli nie ma wierzcholka koncowego w drzewie
			drzewoRozpinajace.insertEdge(pocz, kon, k->getWartosc());
			klasterWierzcholkow.mergeClusters(pocz, kon); //polacz klastry
		}
	}
}

//tworzy minimalne drzewo rozpinajace za pomoca algorytmu Prima
//param [in] graf - graf, ktorego chcemy wyznaczyc MDR
//param [out] drzewoRozpinajace - referencja do minimalnego drzewa rozpinajacego grafu
template <class typGrafu>
void Prima(const typGrafu & graf, typGrafu & drzewoRozpinajace)
{
	int iloscWierzcholkow = graf.getWierzcholki().size();
	kolejka <krawedz<int, int>*> kol(graf.getKrawedzie().size() + 1);  //kolejka do sortowania krawedzi
	lista <krawedz<int, int>*> krawedzieIncydentne;  //lista krawedzi incydentnych
	bool * odwiedzone = new bool[iloscWierzcholkow + 1]; //tablica przechowujaca indeksy wierzcholkow, ktore byly juz odiwedzone

	for (int i = 0; i <= iloscWierzcholkow; i++) //inicjalizacja tablicy odiwedzen
		odwiedzone[i] = false;

	krawedz <int, int> * krawPom; //krawedz pomocnicza
								  //losowy wierzcholek poczatkowy
	int wierzPom = graf.getWierzcholki().atIndex(rand() % iloscWierzcholkow)->getWartosc();
	int wierzSasiedni; //wierzcholek sasiedni do wierzPom w grafie
	odwiedzone[wierzPom] = true;

	wierzcholek <int, int> * tmpWierz = graf.getWierzcholki().atIndex(0); //wierzcholek tymczasowy do petli
	for (int i = 0; i < iloscWierzcholkow; i++) { //dodanie wszystkich wierzcholkow do MDR
		drzewoRozpinajace.insertVertex(tmpWierz->getWartosc());
		tmpWierz = tmpWierz->nextVertex();
	}

	for (int i = 1; i < iloscWierzcholkow; i++) {
		graf.incidentEdges(wierzPom, krawedzieIncydentne);  //zapisanie krawedzi wychodzacych z wierzcholka do listy krawedzieIncydentne
		while (!krawedzieIncydentne.empty()) {
			krawPom = krawedzieIncydentne.pop_front();
			wierzSasiedni = graf.opposite(wierzPom, krawPom);
			if (odwiedzone[wierzSasiedni] == false) {  //jesli wierzcholek sasiedni nie byl jeszcze odwiedzony
				kol.insert(krawPom->getWartosc(), krawPom);  //to dodaj krawedz do kolejki
			}
		}
		if (!kol.isEmpty()) {
			do {
				krawPom = kol.front();  //wez krawedz o najmniejszej wartosci
				kol.removeMin();
				wierzSasiedni = graf.opposite(wierzPom, krawPom); //znajdz wierzcho�ek przeciwny wzd�uz tej krawedzi
			} while (odwiedzone[wierzSasiedni] == true); //dop�ki nie znajdziesz nieodwiedzonego
			drzewoRozpinajace.insertEdge(wierzPom, wierzSasiedni, krawPom->getWartosc());
		}
		odwiedzone[wierzSasiedni] = true;
		wierzPom = wierzSasiedni;
		if (!kol.isEmpty()) kol.deleteAll();
	}
}

//oblicza odleg�o�ci wszystkich wierzcho�kow pocz�wszy od wierzcho�ka startowego start
//param [in] graf - graf, w ktorym bedziemy wyznaczali sciezki
//param [in] start - startowy wierzcholek, z ktorego bedziemy wyznaczac sciezki
template <class typGrafu>
void DijkstraDistances(const typGrafu & graf, const wierzcholek <int, int> & start)
{
	int iloscWierzcholkow = graf.getWierzcholki().size();
	kolejka <int> kol(iloscWierzcholkow + 1);  //kolejka do sortowania wierzcholkow
	lista <krawedz<int, int>*> krawedzieIncydentne;  //lista krawedzi incydentnych
	int wierzPom, wierzSasiedni; //wierzcholki pomocnicze w petlach
	krawedz <int, int> * krawPom; //krawedz pomocnicza
	unsigned int odleglosc; //odleglosc sasiedniego wierzcholka
	int * tablicaKosztow = new int[iloscWierzcholkow + 1]; //tablica zawierajaca koszt dojscia do kazdego wierzcholka
														   //indeks tablicy odzwierciedla wierzcho�ek, a wartosc koszt

	for (int i = 1; i <= iloscWierzcholkow; i++) {
		if (i == start.getWartosc()) tablicaKosztow[i] = 0; //jesli wierzchlek startowy
		else tablicaKosztow[i] = INT_MAX;  //ala infinity
		kol.insert(tablicaKosztow[i], i);
	}
	while (!kol.isEmpty()) {
		wierzPom = kol.front(); //wez wierzchlek o najmniejszym koszcie dojscia
		kol.removeMin();
		graf.incidentEdges(wierzPom, krawedzieIncydentne);  //zapisz krawedzie incydentne w liscie
		while (!krawedzieIncydentne.empty()) {  //dla wszystkich krawedzi incydentnnych
			krawPom = krawedzieIncydentne.pop_front();
			wierzSasiedni = graf.opposite(wierzPom, krawPom); //znajdz wierzcholek przeciwny
			odleglosc = tablicaKosztow[wierzPom] + krawPom->getWartosc();
			if (odleglosc < tablicaKosztow[wierzSasiedni]) {  //jesli aktualnie znaleziona sciezka jest tansza
				tablicaKosztow[wierzSasiedni] = odleglosc;  //zamien koszt dojscia do tego wierzcholka
				kol.replaceKey(kol.indexOf(wierzSasiedni), odleglosc); //dodaj zmieniona wartosc do kolejki
			} //if
		} //while
	} //while
}

//oblicza srednia elementow tablicy
//param [in] tab - tablica z danymi
//return srednia elementow tablicy
double sredniaElementowTablicy(double tab[], int iloscElementow)
{
	double suma = 0;
	for (int i = 0; i < iloscElementow; i++)
		suma += tab[i];
	return suma / iloscElementow;
}

//generuje wszystkie pliki z grafami, dla wszystkich mozliwosci
template <class typGrafu>
void testy(int iloscGrafow, vector <int> liczbaWierzcholkow, vector <float> gestosc, typAlgorytmu alg)
{
	string nazwa;
	ifstream plikWe;     //uchyt do pliku
	ofstream wyniki;     //uchwyt do pliku z wynikami pomiarow czasow sortowan
	Timer timer;         //stoper
	double * wynikiPomiarow = new double[iloscGrafow];  //tablica z danymi dla wszystkich pomiarow danego rodzaju danych

	otworzDoZapisu(wyniki, "wyniki.txt");
	switch (alg)    //wstawienie odpowiedniego tytulu w pliku
	{
	case kruskal:
		wyniki << endl << "==========================================" << endl;
		wyniki << "Czasy dla algorytmu Kruskala:" << endl;
		wyniki << "==========================================" << endl;
		break;
	case prima:
		wyniki << endl << "==========================================" << endl;
		wyniki << "Czasy dla algorytmu Prima:" << endl;
		wyniki << "==========================================" << endl;
		break;
	case dijkstra:
		wyniki << endl << "==========================================" << endl;
		wyniki << "Czasy dla algorytmu Dijskrta:" << endl;
		wyniki << "==========================================" << endl;
		break;
	default:
		cerr << "Nie powinno Ci� tu by�" << endl;
		break;
	}

	for (vector<int>::iterator i = liczbaWierzcholkow.begin(); i != liczbaWierzcholkow.end(); ++i) {
		wyniki << "***************\nLiczba wierzcholkow: " << *i << "\n***************\n";
		for (vector<float>::iterator j = gestosc.begin(); j != gestosc.end(); ++j) {
			wyniki << "Gestosc: " << *j << endl;
			for (int k = 0; k < iloscGrafow; k++) {
				nazwa = to_string(*i) + "_" + to_string(*j) + "(" + to_string(k) + ").txt";  //generowanie nazwy
				typGrafu graf, drzewo;
				wczytajPlik(nazwa, graf);
				switch (alg) {
				case kruskal:
					timer.start();
					Kruskal(graf, drzewo);
					timer.stop();
					wynikiPomiarow[k] = timer.getElapsedTimeInMilliSec();
					break;
				case prima:
					timer.start();
					Prima(graf, drzewo);
					timer.stop();
					wynikiPomiarow[k] = timer.getElapsedTimeInMilliSec();
					break;
				case dijkstra:
					timer.start();
					DijkstraDistances(graf, graf.getWierzcholki().atIndex(graf.findIndxV(3))->getWartosc());
					timer.stop();
					wynikiPomiarow[k] = timer.getElapsedTimeInMilliSec();
					break;
				default:
					cerr << "Nie powinienes tego widziec!" << endl;
					break;
				} //switch
				zamknijPlikDoOdczytu(plikWe);
			} //for - ilosc grafow
			wyniki << "Sredni czas: " << sredniaElementowTablicy(wynikiPomiarow, iloscGrafow) << " ms" << endl;
		} //for - gestosc
	} //for - ilosc wierzcholkow
	zamknijPlikDoZapisu(wyniki);
} //testy

int main()
{
	srand(time(NULL));
	ifstream plikWe;
	ofstream plikWy;
	Timer timer;

	vector <int> liczbaWierzcholkow;
	vector <float> gestosc;
	int iloscGrafow = 10;

	liczbaWierzcholkow.push_back(10);
	liczbaWierzcholkow.push_back(50);
	liczbaWierzcholkow.push_back(100);
	liczbaWierzcholkow.push_back(250);
	liczbaWierzcholkow.push_back(500);
	gestosc.push_back(25);
	gestosc.push_back(50);
	gestosc.push_back(75);
	gestosc.push_back(100);
	timer.start();
	cout << "Generuje pliki... " << endl;
	//generujWszystkiePliki<grafMacierz<int, int>>(iloscGrafow, liczbaWierzcholkow, gestosc);
	cout << "Wygenerowano wszystko!" << endl;
	timer.stop();
	cout << endl << "czas generowania: " << timer.getElapsedTimeInSec() << " s" << endl;

	timer.start();
	cout << "Przeprowadzam testy agorytmow" << endl;
	cout << "Po zakonczonych testach wyniki zostana zapisane do pliku wyniki.txt" << endl;
	//testy<grafMacierz<int,int>>(iloscGrafow, liczbaWierzcholkow, gestosc, kruskal);
	//testy<grafMacierz<int, int>>(iloscGrafow, liczbaWierzcholkow, gestosc, prima);
	//testy<grafMacierz<int, int>>(iloscGrafow, liczbaWierzcholkow, gestosc, dijkstra);
	cout << "Wyniki testow zapisano w pliku: wyniki.txt" << endl;
	timer.stop();
	cout << endl << "czas testow: " << timer.getElapsedTimeInSec() << " s";


	grafMacierz <int, int> graf;
	wczytajPlik<grafMacierz <int, int>>("500_100.000000(0).txt", graf);
	cout << endl;
	cout << endl;
	grafMacierz<int, int> drzewo;

	//Kruskal(graf, drzewo);
	Prima(graf, drzewo);

	timer.start();
	DijkstraDistances(graf,graf.getWierzcholki().atIndex(graf.findIndxV(5))->getWartosc());
	timer.stop();
	cout << "czas: " << timer.getElapsedTimeInMilliSec() << endl;
	for (int i = 0; i < drzewo.getKrawedzie().size(); i++) {
	int * tab = drzewo.endVertices(drzewo.getKrawedzie().atIndex(i));
	cout << tab[0] << " " << tab[1] << ":"
	<< drzewo.getKrawedzie().atIndex(i)->getWartosc() << endl;
	}
	cout << "\n\nIlosc krawedzi drzewa: " << drzewo.getKrawedzie().size() << endl;

	cin.get();
	return 0;
}
