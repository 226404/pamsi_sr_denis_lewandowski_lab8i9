//lista.h
#pragma once
#include <iostream>
#include <exception>

template <class typ>
class element
{
public:
	typ wartosc;
	element * nastepny;
	element() {
		wartosc = 0;
		nastepny = nullptr;
	}
};

class EmptyListException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Lista jest pusta";
	}
};

class SizeListException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Przekroczono rozmiar listy";
	}
};

template <class typ>
class lista
{
	element <typ> * glowa;    //pierwszy element listy
	element <typ> * ogon;     //drugi element listy
	int rozmiar;

public:
	lista() : rozmiar(0) {
		glowa = nullptr;
		ogon = nullptr;

	}

	~lista() {
		element <typ> * tmp;
		while (glowa != nullptr) {
			tmp = glowa;
			glowa = glowa->nastepny;
			rozmiar--;
			delete tmp;
		}
	}

	element <typ>* get_glowa() const {
		return glowa;
	}

	element <typ>* set_glowa() {
		return glowa;
	}

	element <typ>* get_ogon() const {
		return ogon;
	}

	element <typ>* set_ogon() {
		return ogon;
	}

	//zwraca rozmiar listy
	int size() const {
		return rozmiar;
	}

	//sprawdza czy lista jest pusta
	//true - jest pusta
	//false - nie jest pusta
	bool empty() const {
		if (glowa == nullptr) return true;
		else return false;
	}

	//dodaje element na pocz�tek listy
	void push_front(const typ wart);

	//dodaje element na koniec listy
	element <typ>* push_back(const typ wart);

	//dodaje element w wybranym miejscu
	//param[in] wart - warto�� jak� chcemy doda� do listy
	//param[in] indx - index w kt�rym chcemy doda� element (0 - pierwsze miejsce)
	void insert(const typ & wart, const int indx);

	//usuwa element z pocz�tku listy
	typ pop_front();

	//usuwa element z ko�ca listy
	typ pop_back();

	//zwraca pierwszy element listy, ale go nie usuwa
	typ front();

	//usuwa element z wybranego miejsca
	//param[in] indx - index listy, kt�ry chcemy usun�� (0 - pierwsze miejsce)
	typ del(const int indx);

	//wy�wietla ca�� list� na ekran
	void show() const;

	//usuwa ca�a list�
	void deleteAll();

	//zwraca element listy o podanym indeksie (0 - pierwszy)
	typ & operator [] (int indx) const;

	//zwraca indeks podanego elementu
	int indexOf(typ w);

	//zwraca element o podanym indeksie
	typ & atIndex(int indx) const;
};

template <class typ>
void lista <typ>::push_front(const typ wart)
{
	element <typ> * nowy = new element <typ>;
	nowy->wartosc = wart;
	nowy->nastepny = nullptr;
	if (empty()) {
		glowa = nowy;
		ogon = nowy;
	}
	else {
		nowy->nastepny = glowa;
		glowa = nowy;
	}
	rozmiar++;
}

template <class typ>
element<typ> * lista <typ>::push_back(const typ wart)
{
	element <typ> * nowy = new element <typ>;
	nowy->wartosc = wart;
	nowy->nastepny = nullptr;
	if (empty()) {
		glowa = nowy;
		ogon = nowy;
	}
	else {
		ogon->nastepny = nowy;
		ogon = nowy;
	}
	rozmiar++;
	return nowy;
}

template <class typ>
void lista <typ>::insert(const typ & wart, const int indx)
{
	if (indx == 0) push_front(wart);
	else {
		if (indx > rozmiar) {
			throw SizeListException();
		}
		else {
			if (indx == rozmiar) push_back(wart);
			else {
				element <typ> * nowy = new element <typ>;
				element <typ> * tmp = glowa;
				nowy->wartosc = wart;
				for (int i = 0; i < indx - 1; i++) {
					tmp = tmp->nastepny;
				}
				nowy->nastepny = tmp->nastepny;
				tmp->nastepny = nowy;
				rozmiar++;
			}
		}
	}
}

template <class typ>
typ lista <typ>::pop_front()
{
	typ wartosc;
	if (empty()) {
		throw EmptyListException();
	}
	else {
		element <typ> * tmp = glowa;
		glowa = glowa->nastepny;
		wartosc = tmp->wartosc;
		delete tmp;
		rozmiar--;
	}
	return wartosc;
}

template <class typ>
typ lista <typ>::pop_back()
{
	element <typ> * tmp = glowa;
	typ wartosc;
	if (empty()) {
		throw EmptyListException();
	}
	else {
		if (rozmiar == 1) {
			wartosc = glowa->wartosc;
			delete glowa;
			rozmiar--;
			glowa = nullptr;
			ogon = nullptr;
		}
		else {
			for (int i = 2; i<rozmiar; i++) {
				tmp = tmp->nastepny;
			}
			wartosc = ogon->wartosc;
			delete ogon;
			ogon = tmp;
			ogon->nastepny = nullptr;
			rozmiar--;
		}
	}
	return wartosc;
}

template<class typ>
typ lista<typ>::front()
{
	if (empty()) throw EmptyListException();
	else return glowa->wartosc;
}

template <class typ>
typ lista <typ>::del(const int indx)
{
	typ wartosc = 0;

	if (indx == 0)  pop_front();
	else {
		if (indx >= rozmiar) {
			throw SizeListException();
		}
		else {
			if (indx == rozmiar - 1) pop_back();
			else {
				element <typ> * tmp = glowa;
				element <typ> * tmp2 = glowa->nastepny;
				for (int i = 1; i < indx; i++) {
					tmp = tmp->nastepny;
					tmp2 = tmp2->nastepny;
				}
				tmp->nastepny = tmp2->nastepny;
				wartosc = tmp2->wartosc;
				delete tmp2;
				rozmiar--;
			}
			return wartosc;
		}
	}

}

template <class typ>
void lista <typ>::show() const
{
	if (empty()) {
		throw EmptyListException();
	}
	else {
		element <typ> * tmp = glowa;
		while (tmp != nullptr) {
			std::cout << tmp->wartosc << std::endl;
			tmp = tmp->nastepny;
		}
	}
}

template <class typ>
void lista <typ>::deleteAll()
{
	element <typ> * tmp;
	if (empty()) {
		throw EmptyListException();
	}
	else {
		while (glowa != nullptr) {
			tmp = glowa;
			glowa = glowa->nastepny;
			delete tmp;
			rozmiar--;
		}
		ogon = nullptr;
	}
}

template<class typ>
typ & lista<typ>::operator[](int indx) const
{
	element <typ> * tmp = glowa;
	if (indx < 0 || indx >= rozmiar) throw SizeListException();
	else {
		for (int i = 0; i < indx; i++) tmp = tmp->nastepny;
		return tmp->wartosc;
	}
}

template<class typ>
int lista<typ>::indexOf(typ w)
{
	int i = 0;
	element <typ> * tmp = glowa;
	while (tmp->wartosc != w && tmp != nullptr) {
		tmp = tmp->nastepny;
		i++;
	}
	if (tmp == nullptr) {
		cerr << "Nie znaleziono elementu" << endl;
		return -1;
	}
	return i;
}

template<class typ>
typ & lista<typ>::atIndex(int indx) const
{
	element <typ> * tmp = glowa;
	if (indx < 0 || indx >= rozmiar) throw SizeListException();
	else {
		for (int i = 0; i < indx; i++) tmp = tmp->nastepny;
		return tmp->wartosc;
	}
}

template <class typ>
bool operator == (const lista <typ> & a, const lista <typ> & b)
{
	element <typ> * pierwszy = a.get_glowa();
	element <typ> * drugi = b.get_glowa();
	while (pierwszy != nullptr && pierwszy == drugi) {
		pierwszy = pierwszy->nastepny;
		drugi = drugi->nastepny;
	}
	return !pierwszy;
}