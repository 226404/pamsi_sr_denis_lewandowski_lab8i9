#pragma once
#include <exception>
#include "lista.h"

using namespace std;

template <class typWierzcholka, class typKrawedzi> class wierzcholek;
template <class typWierzcholka, class typKrawedzi> class krawedz;
template <class typWierzcholka, class typKrawedzi> class grafLista;

/**********************************************************************************/
class UndefinedConnectionException :public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podane elementy nie sa ze soba powiazane";
	}
};

class AlreadyExistException :public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podany element juz istnieje";
	}
};

class DoesNotExistException :public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Element nie istnieje";
	}
};

class BeginNotExistException :public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Wezel poczatkowy nie istnieje";
	}
};

class EndNotExistException :public exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Wezel koncowy nie istnieje";
	}
};
/*************************************************************************************/

template <class typWierzcholka, class typKrawedzi>
class wierzcholek
{
	typWierzcholka wartosc;
	lista <krawedz <typWierzcholka, typKrawedzi>*> listaSasiedztwa;
	element <wierzcholek<typWierzcholka, typKrawedzi>*>* elem; //wska�nik na miejsce w liscie wierzcholkow
public:
	wierzcholek(typWierzcholka w = 0) : wartosc(w) {}
	const typWierzcholka getWartosc() const { return wartosc; }
	const typWierzcholka getWartosc() { return wartosc; }

	wierzcholek <typWierzcholka, typKrawedzi> * nextVertex() { if (elem->nastepny) return elem->nastepny->wartosc; }

	friend class krawedz <typWierzcholka, typKrawedzi>;
	friend class grafLista <typWierzcholka, typKrawedzi>;
};

template <class typWierzcholka, class typKrawedzi>
class krawedz
{
	typKrawedzi wartosc;
	wierzcholek <typWierzcholka, typKrawedzi> *poczatek; //wska�nik na pocz�tkowy weze�
	wierzcholek <typWierzcholka, typKrawedzi> *koniec; //wska�nik na koncowy wierzcholek
	lista <krawedz<typWierzcholka, typKrawedzi>*> *I_pocz; //wska�nik na element na liscie sasiedztwa poczatkowego wezla
	lista <krawedz<typWierzcholka, typKrawedzi>*> *I_kon; //wskaznik na element na liscie sasiedztwa koncowego wezla
	element <krawedz<typWierzcholka, typKrawedzi>*>* elem; //wska�nik na miejsce w liscie krawedzi
public:
	krawedz(typKrawedzi w = 0, wierzcholek<typWierzcholka, typKrawedzi>*pocz = 0, wierzcholek <typWierzcholka, typKrawedzi>*kon = 0) :
		wartosc(w), poczatek(pocz), koniec(kon) {}
	const typKrawedzi getWartosc() const { return wartosc; }
	const typKrawedzi getWartosc() { return wartosc; }
	const wierzcholek <typWierzcholka, typKrawedzi> *getPoczatek() { return poczatek; }
	const wierzcholek <typWierzcholka, typKrawedzi> *getKoniec() { return koniec; }

	krawedz <typWierzcholka, typKrawedzi> * nextEdge() { if (elem->nastepny) return elem->nastepny->wartosc; }

	friend class wierzcholek <typWierzcholka, typKrawedzi>;
	friend class grafLista <typWierzcholka, typKrawedzi>;
};

template <class typWierzcholka, class typKrawedzi>
class grafLista
{
	lista <wierzcholek<typWierzcholka, typKrawedzi>*> wierzcholki; //lista wez�ow grafu
	lista <krawedz<typWierzcholka, typKrawedzi>*> krawedzie;  //lista krawedzi grafu

public:
	grafLista() { }
	const lista <krawedz<typWierzcholka, typKrawedzi>*> & getKrawedzie() const { return krawedzie; }
	const lista <krawedz<typWierzcholka, typKrawedzi>*> & getKrawedzie() { return krawedzie; }
	const lista <wierzcholek<typWierzcholka, typKrawedzi>*> & getWierzcholki() const { return wierzcholki; }
	const lista <wierzcholek<typWierzcholka, typKrawedzi>*> & getWierzcholki() { return wierzcholki; }

	//zwraca indeks podanego wezla lub -1, je�li nie znaleziono podanego obiektu
	int findIndxV(const typWierzcholka &obiekt);
	int findIndxV(const typWierzcholka &obiekt) const;
	int findIndxV(const wierzcholek <typWierzcholka, typKrawedzi> * obiekt);
	int findIndxV(const wierzcholek <typWierzcholka, typKrawedzi> * obiekt) const;
	//zwraca indeks podanej krawedzi lub -1, je�li nie znaleziono podanego obiektu
	int findIndxE(const typKrawedzi &obiekt);
	int findIndxE(const typKrawedzi &obiekt) const;
	int findIndxE(const krawedz <typWierzcholka, typKrawedzi> * obiekt);
	int findIndxE(const krawedz <typWierzcholka, typKrawedzi> * obiekt) const;

	//dodaje weze� o podanym obiekcie do grafu
	void insertVertex(const typWierzcholka &obiekt);
	//usuwa wierzcholek zawierajacy podany obiekt wraz ze wszystkimi powiazanymi krawedziami
	void removeVertex(const typWierzcholka &obiekt);
	//dodaje krawedz (pocz,kon) zawieraj�c� obiekt
	void insertEdge(const typWierzcholka &pocz, const typWierzcholka &kon, const typKrawedzi & obiekt);
	//usuwa podany wierzcholek z grafu
	void removeEdge(const typKrawedzi & obiekt);
	//zwraca tablice dwoch koncowych wierzcholkow krawedzi
	typWierzcholka * endVertices(const typKrawedzi &obiekt);
	typWierzcholka * endVertices(const krawedz <typWierzcholka, typKrawedzi> *obiekt);
	//zwraca wartosc przeciwleg�ego wierzcho�ka do "w" wzlgedem "k"
	typWierzcholka opposite(const typWierzcholka & w, const typKrawedzi & k);
	typWierzcholka opposite(const typWierzcholka & w, const krawedz <typWierzcholka, typKrawedzi> *k) const;
	//sprawdza, czy wierzcholki v i w sa sasiednie
	bool areAdjacent(const typWierzcholka & v, const typWierzcholka & w);
	//zastepuje element w wierzcholku "w" na "x"
	void replaceVertex(const typWierzcholka & w, const typWierzcholka & x);
	//zastepuje element w krawedzi "k" na "x"
	void replaceEdge(const typKrawedzi & k, const typKrawedzi &x);
	//wyswietla wszystkie wierzcholki na standardowe wyjscie
	void vertices();
	//wyswietla wszystkie krawedzie na standardowe wyjscie
	void edges();
	//wyswietla wszystkie krawedzie przylegajace do wierzcholka "w"
	void incidentEdges(const typWierzcholka & w);
	//zapisuje wszystkie krawedzie przylegajace do wierzcholka "w" do listy "l"
	void incidentEdges(const typWierzcholka & w, lista<krawedz<int, int>*>& l) const;
};

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxV(const typWierzcholka & obiekt)
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]->wartosc) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxV(const typWierzcholka & obiekt) const
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]->wartosc) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxV(const wierzcholek<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxV(const wierzcholek<typWierzcholka, typKrawedzi>* obiekt) const
{
	int i = 0;
	for (i; i < wierzcholki.size(); i++)
		if (obiekt == wierzcholki[i]) break;
	if (i >= wierzcholki.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
inline int grafLista<typWierzcholka, typKrawedzi>::findIndxE(const typKrawedzi & obiekt)
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]->wartosc) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxE(const typKrawedzi & obiekt) const
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]->wartosc) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxE(const krawedz<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
int grafLista<typWierzcholka, typKrawedzi>::findIndxE(const krawedz<typWierzcholka, typKrawedzi>* obiekt) const
{
	int i = 0;
	for (i; i < krawedzie.size(); i++)
		if (obiekt == krawedzie[i]) break;
	if (i >= krawedzie.size()) return -1;  //jesli przeszukano cala liste
	return i;
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::insertVertex(const typWierzcholka & obiekt)
{
	if (findIndxV(obiekt) < 0) { //jesli nie znaleziono wierzcho�ka w liscie
		wierzcholek<typWierzcholka, typKrawedzi> * nowy = new wierzcholek <typWierzcholka, typKrawedzi>(obiekt);
		element <wierzcholek<typWierzcholka, typKrawedzi>*>* tmp = wierzcholki.push_back(nowy);
		nowy->elem = tmp; //dodanie wskaznika na element w liscie
	}
	else throw AlreadyExistException();
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::removeVertex(const typWierzcholka & obiekt)
{
	int i = findIndxV(obiekt);
	if (i < 0) throw DoesNotExistException();  //jesli nie znaleziono wierzcho�ka w liscie
	else {
		while (!wierzcholki[i]->listaSasiedztwa.empty()) {  //dop�ki lista sasiedztwa nie jest pusta
			typKrawedzi tmp = wierzcholki[i]->listaSasiedztwa.pop_front()->wartosc;  //usun element z przodu
			krawedzie.del(findIndxE(tmp)); //usun krawedz
		}
		wierzcholki.del(i);
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::insertEdge(const typWierzcholka & pocz, const typWierzcholka & kon, const typKrawedzi & obiekt)
{
	int indxPocz = findIndxV(pocz);
	int indxKon = findIndxV(kon);
	krawedz<typWierzcholka, typKrawedzi> *nowy = new krawedz <typWierzcholka, typKrawedzi>;
	nowy->wartosc = obiekt;
	if (indxPocz < 0) throw BeginNotExistException();
	else if (indxKon < 0) throw EndNotExistException();
	else {
		nowy->poczatek = wierzcholki[indxPocz]; //ustawienie wezla poczatkowego
		nowy->koniec = wierzcholki[indxKon]; //ustawienie wezla koncowego
		wierzcholki[indxPocz]->listaSasiedztwa.push_back(nowy); //dodanie informacji o krawedzi na liscie Sasiedztwa wezla poczatkowego
		wierzcholki[indxKon]->listaSasiedztwa.push_back(nowy); //dodanie informacji o krawedzi na liscie Sasiedztwa wezla koncowego
		nowy->I_pocz = &wierzcholki[indxPocz]->listaSasiedztwa; //wskaznik krawedzi na liste Sasiedztwa wezla poczatkowego
		nowy->I_kon = &wierzcholki[indxKon]->listaSasiedztwa; //wskaznik krawedzi na liste sasiedztwa wezla koncowego
		element <krawedz<typWierzcholka, typKrawedzi>*>* tmp = krawedzie.push_back(nowy); //dodanie krawedzi do listy
		nowy->elem = tmp; //dodanie wskaznika na element w liscie
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::removeEdge(const typKrawedzi & obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		krawedz <typWierzcholka, typKrawedzi> * tmp = krawedzie[i];
		int indxListySasiedztwa = tmp->I_pocz->indexOf(tmp); //znalezienie indeksu elementu w liscie sasiedztwa wskazujacego na krawedz
		tmp->I_pocz->del(indxListySasiedztwa); //usuniecie tego elementu z listy
		indxListySasiedztwa = tmp->I_kon->indexOf(tmp);
		tmp->I_kon->del(indxListySasiedztwa);
		krawedzie.del(i);
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka * grafLista<typWierzcholka, typKrawedzi>::endVertices(const typKrawedzi & obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		typWierzcholka * tab = new typWierzcholka[2];
		tab[0] = krawedzie[i]->poczatek->wartosc;
		tab[1] = krawedzie[i]->koniec->wartosc;
		return tab;
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka * grafLista<typWierzcholka, typKrawedzi>::endVertices(const krawedz<typWierzcholka, typKrawedzi>* obiekt)
{
	int i = findIndxE(obiekt);
	if (i < 0) throw DoesNotExistException();
	else {
		typWierzcholka * tab = new typWierzcholka[2];
		tab[0] = krawedzie[i]->poczatek->wartosc;
		tab[1] = krawedzie[i]->koniec->wartosc;
		return tab;
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka grafLista<typWierzcholka, typKrawedzi>::opposite(const typWierzcholka & w, const typKrawedzi & k)
{
	int indxW = findIndxV(w);
	int indxK = findIndxE(k);
	if (indxW < 0 || indxK < 0) DoesNotExistException();
	else {
		if (krawedzie[indxK]->poczatek->wartosc == w) return krawedzie[indxK]->koniec->wartosc;
		else if (krawedzie[indxK]->koniec->wartosc == w) return krawedzie[indxK]->poczatek->wartosc;
		else throw UndefinedConnectionException(); //podany wierzcholek i krawedz istnieja, ale nie sa powiazane
	}
}

template<class typWierzcholka, class typKrawedzi>
typWierzcholka grafLista<typWierzcholka, typKrawedzi>::opposite(const typWierzcholka & w, const krawedz<typWierzcholka, typKrawedzi>* k) const
{
	if (k->poczatek->wartosc == w) return k->koniec->wartosc;
	else if (k->koniec->wartosc == w) return k->poczatek->wartosc;
	else throw UndefinedConnectionException(); //podany wierzcholek i krawedz istnieja, ale nie sa powiazane
}

template<class typWierzcholka, class typKrawedzi>
bool grafLista<typWierzcholka, typKrawedzi>::areAdjacent(const typWierzcholka & v, const typWierzcholka & w)
{
	int indxV = findIndxV(v);
	int indxW = findIndxV(w);
	if (indxV < 0 || indxW < 0) throw DoesNotExistException();
	else {
		lista <krawedz<typWierzcholka, typKrawedzi>*> *tmp = &wierzcholki[indxV]->listaSasiedztwa;
		//sprawdzenie calej listy sasiedztwa wierzcholka "v" czy wsrod jego krawedzi
		//wystepuje wskaznik na wierzcholek "w"
		for (int i = 0; i < tmp->size(); i++)
			if (tmp->atIndex(i)->poczatek->wartosc == w || tmp->atIndex(i)->koniec->wartosc == w) return true;
	}
	return false;
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::replaceVertex(const typWierzcholka & w, const typWierzcholka & x)
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else wierzcholki[i]->wartosc = x;
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::replaceEdge(const typKrawedzi & k, const typKrawedzi & x)
{
	int i = findIndxE(k);
	if (i < 0) throw DoesNotExistException();
	else krawedzie[i]->wartosc = x;
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::vertices()
{
	for (int i = 0; i < wierzcholki.size(); i++)
		cout << wierzcholki[i]->wartosc << " ";
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::edges()
{
	for (int i = 0; i < krawedzie.size(); i++)
		cout << krawedzie[i]->wartosc << " ";
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::incidentEdges(const typWierzcholka & w)
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else {
		lista <krawedz<typWierzcholka, typKrawedzi>*> *tmp = &wierzcholki[i]->listaSasiedztwa;
		for (int j = 0; j < tmp->size(); j++)
			cout << tmp->atIndex(j)->wartosc << " ";
	}
}

template<class typWierzcholka, class typKrawedzi>
void grafLista<typWierzcholka, typKrawedzi>::incidentEdges(const typWierzcholka & w, lista<krawedz<int, int>*>& l) const
{
	int i = findIndxV(w);
	if (i < 0) throw DoesNotExistException();
	else {
		lista <krawedz<typWierzcholka, typKrawedzi>*> *tmp = &wierzcholki[i]->listaSasiedztwa;
		for (int j = 0; j < tmp->size(); j++)
			l.push_back(tmp->atIndex(j));
	}
}