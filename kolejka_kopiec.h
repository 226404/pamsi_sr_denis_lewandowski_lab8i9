#pragma once 
#include <iostream>

//klasy
template <class typ>
class wagonik
{
	typ wart;
	int klucz;
	wagonik()
	{}
	wagonik(typ e, int k)
	{
		wart = e;
		klucz = k;
	}

	template <class typ>
	friend class kolejka;
};

template <class typ>
class kolejka
{
	wagonik<typ> *tab;
	int l;

public:
	kolejka(int n)
	{
		tab = new wagonik<typ>[n];            // Tworzymy tablic�
		l = 0;                       // Pozycja w kopcu
	}
	~kolejka()
	{
		delete[] tab;
	}
	//sprawdza czy kolejka jest pusta
	//true - je��li jest pusta
	//false - gdy nie jest pusta
	bool isEmpty()
	{
		if (l == 0)
			return true;
		return false;
	}
	//zwraca ilosc dodanych elementow kolejki
	int size() { return l; }
	//zwraca pierwszy element kopca, ale go nie usuwa
	typ front() { return tab[0].wart; }
	//dodaje element e w odpowiednie miejsce na podstawie klucza k
	int insert(int k, typ & e);
	//usuwa pierwszy element z kopca
	void removeMin();
	//wyswietla wartosci w kolejce na standardowe wyj�cie
	void show();
	//zamienia wartosc klucza w elemencie o podanym indeksie
	int replaceKey(int i, int k);
	//zwraca wartosc elementu o podanym indeksie
	typ atIndex(int x) { return tab[x].wart; }
	//zwraca indeks elementu zawierajacy podana wartosc
	int indexOf(typ & e);
	//"usuwa" cala kolejke
	void deleteAll() { l = 0; }
};

template <class typ>
int kolejka<typ>::insert(int k, typ & e)
{
	int i, j;
	wagonik<typ> nowy(e, k);
	i = l++;     //i na ostatnim miejscu kopca
	j = (i - 1) / 2; // ojciec dla i

	while (i > 0 && (tab[j].klucz > nowy.klucz)) // dopoki nie dojdziemy do korzenia oraz klucz ojca jest za duzy
	{
		tab[i] = tab[j]; // przesuwamy element z ojca do syna
		i = j; // ojcies zostaje synem
		j = (i - 1) / 2; // ojciec ojca staje sie ojcem
	}

	tab[i] = nowy; //dodajemy element do kopca
	return i;
}

template <class typ>
void kolejka<typ>::show()
{
	cout << "Zawartosc kolejki: " << endl;
	for (int i = 0; i < l; i++)
		cout << "[" << i << "] " << tab[i].wart->wartW() << " Droga: " << tab[i].klucz << " Miejsce: " << tab[i].wart->zm2W() << endl;
}

template <class typ>
int kolejka<typ>::replaceKey(int i, int k)
{
	wagonik<typ> tmp;
	int j;
	tab[i].klucz = k;

	if (tab[(i - 1) / 2].klucz > tab[i].klucz)
	{
		j = (i - 1) / 2; // ojciec dla i
		while (i > 0 && (tab[j].klucz > tab[i].klucz)) // dopoki nie dojdziemy do korzenia oraz klucz ojca jest za duzy
		{
			tmp = tab[i]; // zapamietujemy zawartosc syna
			tab[i] = tab[j]; // przesuwamy element z ojca do syna
			tab[j] = tmp; // w miejscu ojca w pisujemy syna
			i = j; // ojcies zostaje synem
			j = (i - 1) / 2; // ojciec ojca staje sie ojcem
							 //cout << "!!1" << endl;
		}
	}
	else
	{
		j = 2 * i + 2;; // ojciec ojca staje sie ojcem
		while (j < l && (tab[j].klucz < tab[i].klucz)) // dopoki nie dojdziemy do korzenia oraz klucz ojca jest za duzy
		{
			tmp = tab[i]; // zapamietujemy zawartosc syna
			tab[j] = tmp; // w miejscu ojca wpisujemy syna
			i = j; // ojcies zostaje synem
			j = 2 * i + 2;; // ojciec ojca staje sie ojcem
							//cout << "!!2" << endl;
		}
	}
	return j;
}

template<class typ>
int kolejka<typ>::indexOf(typ & e)
{
	int i = 0;
	while (tab[i].wart != e) i++;
	return i;
}

template <class typ>
void kolejka<typ>::removeMin()
{
	int i, j;
	wagonik<typ> tmp;

	if (l)
	{
		tmp = tab[--l];
		i = 0;
		j = 1;

		while (j < l)
		{
			if ((j + 1 < l) && (tab[j + 1].klucz < tab[j].klucz))
				j++;
			if (tmp.klucz <= tab[j].klucz)
				break;
			tab[i] = tab[j];
			i = j;
			j = 2 * j + 1;
		}
		tab[i] = tmp;
	}
}
